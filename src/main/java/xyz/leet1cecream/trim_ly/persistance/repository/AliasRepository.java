package xyz.leet1cecream.trim_ly.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import xyz.leet1cecream.trim_ly.persistance.model.Alias;

import java.util.Optional;

public interface AliasRepository extends CrudRepository<Alias, Long> {

    Optional<Alias> findByRedirectId(String redirectId);
    Optional<Alias> findByManagementId(String managementId);
    boolean existsByRedirectId(String redirectId);
    boolean existsByManagementId(String managementId);

    @Transactional
    void deleteByManagementId(String managementId);

}
