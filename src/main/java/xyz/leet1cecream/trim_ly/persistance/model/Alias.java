package xyz.leet1cecream.trim_ly.persistance.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;
import org.hibernate.validator.constraints.UUID;

@Getter @Setter
@Entity
@Table(name = "aliases")
public class Alias extends BaseEntity {

    @Column(name = "redirect_id")
    String redirectId;

    @UUID
    @Column(name = "management_id")
    String managementId = java.util.UUID.randomUUID().toString();

    @URL(regexp = "^(https?).*", message = "Invalid URL.")
    @Column(name = "original_url")
    String originalUrl;

    @Column(name = "request_ip")
    String requestIp;

}
