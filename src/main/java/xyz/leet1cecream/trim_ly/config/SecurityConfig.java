package xyz.leet1cecream.trim_ly.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig implements WebMvcConfigurer {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/**"),
                                AntPathRequestMatcher.antMatcher("/**/delete"),
                                /*AntPathRequestMatcher.antMatcher("/signin"),
                                AntPathRequestMatcher.antMatcher("/signup"),
                                AntPathRequestMatcher.antMatcher("/logout"),*/
                                AntPathRequestMatcher.antMatcher("/privacy"),
                                AntPathRequestMatcher.antMatcher("/error"),
                                AntPathRequestMatcher.antMatcher("/css/**"),
                                AntPathRequestMatcher.antMatcher("/js/**"),
                                AntPathRequestMatcher.antMatcher("/images/**"),
                                AntPathRequestMatcher.antMatcher("/favicon.ico")).permitAll()
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/user/links"),
                                AntPathRequestMatcher.antMatcher("/user/changepassword")).authenticated()
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/product/*/**"),
                                AntPathRequestMatcher.antMatcher("/h2-console/**")).hasRole("ADMIN")
                )
                .headers(headers -> headers.frameOptions().disable())
                .csrf(csrf -> csrf.ignoringRequestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).disable())
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error")
                .and()
                .logout()
                .logoutRequestMatcher(AntPathRequestMatcher.antMatcher("/logout"))
                .deleteCookies("JSESSIONID");

        return http.build();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

}
