package xyz.leet1cecream.trim_ly.service;

import xyz.leet1cecream.trim_ly.persistance.model.Alias;

import java.util.Optional;

public interface AliasService {

    Alias createAlias(String url, String ip);
    Optional<Alias> findByRedirectId(String redirectId);
    Optional<Alias> findByManagementId(String managementId);
    boolean existsByManagementId(String managementId);
    void deleteAlias(String managementId);
}
