package xyz.leet1cecream.trim_ly.service;

public interface RedirectIdGenerator {

    String getUniqueId();

}
