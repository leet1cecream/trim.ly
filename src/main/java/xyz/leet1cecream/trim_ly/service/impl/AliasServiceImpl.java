package xyz.leet1cecream.trim_ly.service.impl;

import org.springframework.stereotype.Service;
import xyz.leet1cecream.trim_ly.persistance.model.Alias;
import xyz.leet1cecream.trim_ly.persistance.repository.AliasRepository;
import xyz.leet1cecream.trim_ly.service.AliasService;
import xyz.leet1cecream.trim_ly.service.RedirectIdGenerator;

import java.util.Optional;

@Service
public class AliasServiceImpl implements AliasService {

    private final AliasRepository aliasRepository;

    private final RedirectIdGenerator redirectIdGenerator;

    public AliasServiceImpl(AliasRepository aliasRepository, RedirectIdGenerator redirectIdGenerator) {
        this.aliasRepository = aliasRepository;
        this.redirectIdGenerator = redirectIdGenerator;
    }

    @Override
    public Alias createAlias(String url, String ip) {
        Alias alias = new Alias();
        alias.setOriginalUrl(url);
        alias.setRedirectId(redirectIdGenerator.getUniqueId());
        alias.setRequestIp(ip);
        return aliasRepository.save(alias);
    }

    @Override
    public Optional<Alias> findByRedirectId(String redirectId) {
        return aliasRepository.findByRedirectId(redirectId);
    }

    @Override
    public Optional<Alias> findByManagementId(String managementId) {
        return aliasRepository.findByManagementId(managementId);
    }

    @Override
    public boolean existsByManagementId(String managementId) {
        return aliasRepository.existsByManagementId(managementId);
    }

    @Override
    public void deleteAlias(String managementId) {
        aliasRepository.deleteByManagementId(managementId);
    }
}
