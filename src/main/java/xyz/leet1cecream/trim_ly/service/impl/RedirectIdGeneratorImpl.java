package xyz.leet1cecream.trim_ly.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import xyz.leet1cecream.trim_ly.persistance.repository.AliasRepository;
import xyz.leet1cecream.trim_ly.service.RedirectIdGenerator;

@Service
public class RedirectIdGeneratorImpl implements RedirectIdGenerator {

    private final AliasRepository aliasRepository;

    public RedirectIdGeneratorImpl(AliasRepository aliasRepository) {
        this.aliasRepository = aliasRepository;
    }

    @Override
    public String getUniqueId() {
        String id = RandomStringUtils.randomAlphanumeric(6, 6);
        while (aliasRepository.existsByRedirectId(id)) {
            id = RandomStringUtils.randomAlphanumeric(6, 6);
        }
        return id;
    }
}
