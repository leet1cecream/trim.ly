package xyz.leet1cecream.trim_ly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrimLyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrimLyApplication.class, args);
    }

}
