package xyz.leet1cecream.trim_ly.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import xyz.leet1cecream.trim_ly.persistance.model.Alias;
import xyz.leet1cecream.trim_ly.service.AliasService;

import java.util.Optional;

@Controller
public class IndexController {

    private final AliasService aliasService;

    public IndexController(AliasService aliasService) {
        this.aliasService = aliasService;
    }

    @GetMapping({"", "/"})
    public String index(Model model) {
        model.addAttribute("alias", new Alias());
        return "index";
    }

    @PostMapping("/")
    public String index(@Valid Alias alias, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        if(bindingResult.hasErrors()) {
            return "index";
        }
        Alias savedAlias = aliasService.createAlias(alias.getOriginalUrl(), httpServletRequest.getRemoteAddr());
        return "redirect:/" + savedAlias.getManagementId();
    }

    @GetMapping("/{id}")
    public String idIndex(Model model, @PathVariable("id") String id) {
        Optional<Alias> alias = aliasService.findByRedirectId(id);
        if (alias.isPresent()) {
            return "redirect:" + alias.get().getOriginalUrl();
        }

        alias = aliasService.findByManagementId(id);
        if (alias.isEmpty()) {
            return "redirect:/";
        }

        model.addAttribute("alias", alias.get());
        return "management";

    }

    @GetMapping("/{managementId}/delete")
    public String delete(@PathVariable("managementId") String managementId) {
        if (aliasService.existsByManagementId(managementId)) {
            aliasService.deleteAlias(managementId);
            return "redirect:/?deleted";
        }
        return "redirect:/";
    }

}
