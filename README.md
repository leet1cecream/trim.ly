# Trim.ly
Trim.ly is a URL shortener made using **Spring Boot**, **Spring Security**,
**Spring Data JPA** with **H2 Database** and **Thymeleaf** with **Bootstrap** for the view layer.

[[_TOC_]]

## Screenshots

![index screenshot](images/index.png)
![index deleted screenshot](images/deleted.png)
![management screenshot](images/management.png)

## Run
Clone the repo:
```
$ git clone https://gitlab.com/leet1cecream/trim.ly.git
```

### Run using maven

Start the appliation:
```
$ mvn spring-boot:run
```

### Package with maven and run as a jar

Package the application:
```
$ mvn clean package
```

Run it as a jar:
```
$ java -jar target/Trim_ly*.jar
```

## License
Project is based on **MIT License**. You can read about the license <a href="LICENSE">here</a>.